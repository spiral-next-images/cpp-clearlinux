FROM registry.gitlab.com/spiral-next-images/cpp-clearlinux/base:31530

RUN swupd bundle-add zip unzip curl c-basic git devpkg-util-linux && \
  swupd repair && \
  swupd clean && \
  mkdir -p /app/cmake && \
  curl -kLo /app/cmake.tar.gz 'https://github.com/Kitware/CMake/releases/download/v3.24.0/cmake-3.24.0-linux-x86_64.tar.gz' && \
  cd /app && \
  tar -C cmake --strip-components 1 -xvf cmake.tar.gz && \
  rm -f cmake.tar.gz && \
  ln -sf /app/cmake/bin/cmake /usr/bin/cmake && \
  curl -kLo ninja.zip https://github.com/ninja-build/ninja/releases/download/v1.11.0/ninja-linux.zip && \
  unzip ninja.zip && \
  ln -sf /app/ninja /usr/bin/ninja && \
  rm ninja.zip && \
  git clone --single-branch -b master https://github.com/microsoft/vcpkg.git && \
  cd vcpkg && \
  ./bootstrap-vcpkg.sh && \
  ln -sf /app/vcpkg/vcpkg /usr/bin/vcpkg
  

ENV VCPKG_ROOT /app/vcpkg
ENV VCPKG_FORCE_SYSTEM_BINARIES 1
